﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace ContactsApp
{
    class Contact
    {
        public string firstName { get; set; }
        public string lastName { get; set; }

        public Contact(string firstName, string lastName)
        {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        // Checks if the contact's firstname or lastname contains a search phrase
        public bool ContainsSearchParam(string SearchParam)
        {
            if(this.firstName.ToLower().Contains(SearchParam) || this.lastName.ToLower().Contains(SearchParam))
            {
                return true;
            }
            return false;
        }
    }
}
